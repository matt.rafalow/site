# TechnoSoc: Ask Me Anything (AMA)
#### *Compiled by Matt Rafalow, Daisy Lu, Angéle Christin (last update: 1/27/21)*

## What is AMA?

Ask Me Anything (AMA) is an activity styled from other platforms (like Reddit) where guests are invited to participate in a Q&A guided by questions from the community. AMAs are an opportunity to get to know our members better and to share their answers in archive format more broadly.

## AMAs
### Alex Hanna, PhD (January 27, 2021)

*Alex Hanna is a sociologist and senior research scientist on the Ethical AI Team at Google Research. Before that, she was an Assistant Professor at the Institute of Communication, Culture, Information and Technology at the University of Toronto. Her research centers on origins of the training data which form the informational infrastructure of AI and algorithmic fairness frameworks, and the way these datasets exacerbate racial, gender, and class inequality.*

All right, I’ve had my second cup of coffee, I’ve put on [Chromatica](https://en.wikipedia.org/wiki/Chromatica), and I’m ready to do this. Thanks so much to *Daisy Lu (she/her)*, *Matt Rafalow (he/his)* and *Angèle (she/her)* for inviting me to do this. I’m very honored to be the inaugural AMA question-answerer. I’ll try to do my best!. As for answering questions, I’ll reply to them in the thread but also send them to the channel. Here we go…

__What first got you into the study of digital technology from a social science perspective? What inspired you?__

I’ve been a nerd for years. Since I was 4. I’ve known that I wanted to do something with computers since those early days. Initially, I thought that looked much more like computer science and so I did things like follow computer-ish trends, looked at Best Buy catalogs :satisfied:. I should have learned some programming in high school. So I majored in CS when I got to university. But soon after, I realized that I had a lot more interest in people and the social aspects of technology, rather than the technology itself. 

After undergrad I went to a coffee shop in Chicago (where I was working as a software developer for a web company) and picked up a NYT magazine article on Egyptians using Facebook to organize protest. This was in 2008, before the revolution. So when I got to Wisconsin for sociology, I collected a bunch of data on the Facebook group they used and used machine learning to understand patterns of mobilization. That turned out to be my master’s thesis and my article in [Mobilization](https://alex-hanna.com/static/pdf/Hanna.Moby2013.pdf). It was a real niche area at the time, to study digital stuff. Not a lot of people were doing it in sociology back then. But that’s what put me on that path.

__If you could go back in time and talk to Alex the PhD student, what would you tell her to do differently and what would you tell her do to more of/continue doing?__

Oh boy, this one, LOL. Hindsight being 20/20, I think I’d have spent more time studying sociology of race and ethnicity — which I think a lot of the fairness/AI ethics conversation has a serious deficit -- and trying to do some projects which would have allowed me to do ethnography. My initial dissertation project was this overly ambitious thing where I wanted to do surveys, ethnography, and quanty computational social science of Egyptian revolution trace data. I applied for a Fulbright and, while the US government was fine with it, the repressive Egyptian government surely was not. So I ended up doing a really quanty, methodological dissertation. It was a risk, and my advisor said it was a risk, but I still did it! I probably would have done a dissertation which was much more catholic (in the 2nd definition sense) in terms of method.

I think I’d actually tell student-Alex to keep studying the computational, though. And to keep on doing activism and organizing.

__What was the vibe (for lack of a better term) of  digital scholarship at the time, around 2008?__

There was a lot of it! Just not in sociology. I found weird pockets of community, at, like, ICWSM. I remember complaining to danah boyd at ICWSM in 2011 that I wanted to bring sociologists and she said she tried to drag computer scientists to the big anthro conference. But at the time, AoIR was around, as well as the communications conferences.

Folks knew that this was important, but they were far from the center of the discipline.

__What do you find most challenging about the work you do? Most rewarding?__

Most challenging: dealing with engineers and people who want technical solutions to social problems.

Most rewarding: getting people to realize that social problems are hard for a reason? Haha. But honestly, I think some of the more rewarding parts of the work is the mentorship I get to do and seeing students I work with succeed.

__If you could teach everyone in the AI and technology space one takeaway from the social sciences what would it be? And if you could have all social scientists learn one takeaway from  the AI and technology space, what would it be? (said another way, what is the main thing these two areas can learn from each other?)__

Ohhh I like this one. People in the AI and technology space, I think, need to learn that the technical cannot be divorced from the contextual. But I think that needs more meat on its bones. What I think that means is that the technical can’t be divorced from the institutional. Technical artifacts interact with organizations at multiple levels.
As for social scientists… hrm. This one is harder for me to answer because I’m not sure there’s anything necessarily “foundational” in AI as a research program that social scientists can learn. That sounds shadier than I wanted it to, but I don’t know if there’s another way to put that. Lemme mull on it for a bit.

__As someone with experience both in academia and industry, what would you recommend to PhD students applying to both types of job? I love your research on dataset creation/infrastructures, AI genealogy, and AI and society broadly speaking. What do you think are the biggest gaps in AI research that sociologists can address?__

Thanks for your :heart: :grinning:

First question: people in industry look for different things in a particular candidate. Hiring managers are typically looking for a skill set which can translate easily to industry. *Matt Rafalow (he/his)*, *Iga Kozlowska (she/her, ee-ga)* and others have written a lot on this, so I’ll defer to their excellent pieces on this. Academic departments are usually looking for publications and ability to present and teach. The two aren’t necessarily in opposition, but it depends a lot on where you publish to be legible to both. If you’re looking to try to apply to both, say, UXR positions and iSchool faculty jobs, then you probably want to target publications like CSCW and HCI. But those publications wouldn’t be legible to, say, a sociology department who wants you to publish in journals.

Biggest gaps in AI research: geez, talking to people affected by AI. Understanding institutions where AI operates. *Angèle (she/her)*, Sarah Brayne, *Elizabeth Watkins*, and Madeleine Clare Elish provide some great models of how and where to do this work ethnographically.

__As someone working on such important, mission-driven work, what do you do to re-charge so you have the energy to keep pushing for what matters?__

Oh my gosh. I’m still trying to figure it out. Take breaks. Take time off. Take naps. I nap every day. I used to be fervently against naps. Why. I miss roller derby, but exercise is good. Do something outside of your job where you aren’t thinking of your job. Find solace in community and family.

__I've long wondered but it always felt too weird to ask, but here it goes: what is a "bad Hessian"?__

:laughing: For those of you who aren’t aware, we used to run this computational social science blog called [Bad Hessian](https://badhessian.org/) back in the day. Adam Slez (faculty at UVA) and I started it when we were at Wisconsin. A [hessian matrix](https://en.wikipedia.org/wiki/Hessian_matrix) is used in calculating coefficients for some types of maximum likelihood models. I know very little about how this actually works. Adam does, because he’s much smarter than me in the quantitative dark arts. Anyhow, the R programming language gives you back a cryptic message like “bad hessian” when you don’t have enough data to calculate a something-something regression. There’s a lot of layers to this joke. I can’t believe I’m typing this all.

__What is your setup for work, hardware and software-wise? Feel free to address or avoid important cultural issues like: Vim or Emacs? Spaces or tabs? R or Python?__

Hard hitting questions here. I use a MacBook Pro. Don’t ask me what kind. I have a big monitor. Don’t ask me what kind. Emacs 4eva, although most of the time it’s Visual Studio Code. Spaces, I guess. Python, if I can help it, but I do a lot of work in R for a [project](https://www.ellenberrey.com/research) I’m doing with Ellen Berrey.

__Was industry something you’ve always been interested in? In other words, was industry something you thought about in graduate school or was the decision easier to make after teaching? (A follow-up would be: what advice do you have for students interested in industry) How have the events of the past year (and summer especially) affected your research with respect to inequality?__

I haven’t always been interested in industry, but I knew it was an option. I thought about it when I was in grad school but I didn’t know how to break in. I also didn’t see a lot of sociologists who were able to get into industry. Nobody in my grad program was telling us how to get industry jobs. For advice, gonna defer to the answer above, which defers to others’ blog posts.
I think you’re referring to BLM protests this summer? Which, if that’s the case, it deepens the need to have racial inequality — and specifically, whiteness and the pervasiveness of anti-Blackness — at the center of sociological and technical analysis, especially in conversations around “fairness” and “justice”.

Oh gosh we only have 17 minutes left. I think I’m going to put [Carly Rae Jepsen](https://en.wikipedia.org/wiki/Emotion_(Carly_Rae_Jepsen_album)) on now.

__Do you have any dream projects? If yes, why those projects specifically? What would you want to do with the data/findings?__

In a way, I think my dream is doing what I’m doing now: Focusing on benchmark datasets, understanding everything I can about those datasets, about their histories and trajectories. I think if I could deepen it more, I would partner with a historian of science to do some archival work. In terms of what to do with results? There’s definitely a lot of room for science policy impact here, especially changing the way these datasets are created and evaluated. Also there’s that book that’s in the offing…

You have traversed many disciplines. You started out in CS, mathematics, and sociology; then focused in sociology as a graduate student; and as an assistant prof and now industry researcher you increasingly publish in HCI. Can you share a bit about this journey, including what shaped this path and what you have learned along the way?

This question is so broad :laughing:. I think my journey has been a weird one, like a careening, silly kind of gradient descent. I’ve learned a lot about what are not acceptable local minima for me: being purely in industry from an engineering perspective (I interned at Microsoft in 2006 and it was awful. Hopefully if I ever apply for MSR they don’t find my exit interview). I was faculty and it was better. But now I landed in a place which feels good and right for now (at least, it did before Jeff Dean messed everything up). I think I learned that your first record doesn’t have to be your best record, that a job is a job, and you gotta find a place that appreciates you for how you work and who you want to talk to.

__In considering what PhD programs to apply to, I've gotten advice from my PI that it's better to establish your expertise in one discipline before you start being interdisciplinary (particularly because people in academia and industry want to know what you specialize in). Do you have advice for anyone who feels lost finding their disciplinary home?__

I feel this intimately. I think you do need a home. I would suggest talking to people across disciplines, taking classes in different disciplines, really trying to get the feel of that discipline. Although this isn’t even a satisfying answer to myself… So maybe some better advice is… find the people who are doing the stuff you want to be doing. Talk to them. Ask them whether their path is one you could/should emulate.

__How do you organize your days? Which series do you watch (it's an AMA right?). And what's the deal with ROLLER DERBY??? We all want to know (and imitate) :heart_eyes_cat:__

Before the mess with my team (and when I was being good to myself and self-blocking Twitter), I was doing a very good job at waking up around 5 or 6 and writing until about 9. Then taking whichever meetings after that, and finishing up around 4 or 5. I’d then workout after that or have roller derby practice.

What do you want to know about roller derby??? It’s a silly ass sport where you put wheely shoes on your feet and hit other people. Find me playing [here](https://www.youtube.com/watch?v=OEahntLSwL4) under my nom du skate Kate Silver.

I just finished Star Trek Voyager and have moved on to Star Trek Enterprise. I am trying to be a completist and watch every Trek (except for The Original Series, which is Bad).

***

![bookshelf with closeup of books](/img/books.jpg)
Photo by [Ugur Akdemir](https://unsplash.com/@ugur)
