# `sociologists.digital` Guide

## Table of Contents

1. [Purpose](#purpose)
2. [How It Works](#how_it_works)
3. [Adding Pages](#adding_pages)
4. [Resources](#resources)

## Purpose <a name="purpose"></a>

Our goal is for the [sociologists.digital](https://sociologists.digital/) site to be a central place for resources: a place where people can sign up to join the Slack and a more permanent home for community initiatives such as AMAs.

This guide is to help those who aren't as familiar with Git learn how to contribute to the site.

## How It Works <a name="how_it_works"></a>

Gitlab hosts the site and this repository has both the content and code that dictate what the site will look like. The main folder you will interact with is the `pages/` folder, but the bullets below provide a big picture of how it all fits together.

* `assets/` contains the style information, fonts, icons and scripts for the site.
* `img/` contains images.
* `pages/` contains the markdown files that are the content of each page.
* `templates/` contains the HTML for the site (connects the style information and any Javascript, or dynamic, components of the site).
* `build.py` is like the "connective tissue" between the markdown files in `pages/` and the site. It formats the markdown files into something readable and "feeds" them into the HTML pages.
* `default.nix` manages project requirements. It's the technical specification of all the packages that are needed to build the website.
* `.gitlab-ci.yml` gives Gitlab the instructions on how to deploy the site (deploying just means making the site live and accessible).

## Adding Pages <a name="adding_pages"></a>

To edit right in your browser, go to the `Web IDE` option.

![menu](/img/gitlab_menu.png)

Working in the IDE:

* __On the left panel__, you can upload files directly to the repo. To keep things organized, images for your post should be uploaded to `/img/`. We recommend naming these files without any spaces (e.g. "gitlab_ide.png" rather than "GitLab IDE.png").
* __On the right panel__, you can edit your markdown file in `Edit`, using markdown syntax. `Preview Markdown` will show you the formatted version. If you are adding a new page to the website, within the `pages` folder, create a new markdown file. For example, to create an AMA page, you would make a new file called `amas.md`.
* __Markdown syntax__: if you're lost on how to use markdown, you can check the [Resources](#resources) section or you could even just look at other markdown files already uploaded like this one.
* __Adding images__: When you add an image to a markdown page, the syntax to do this is `![caption](/img/picture_name.png)`.
* __Adding emojis__: To add an emoji, you can use the short text codes, like this: :smile: (`:smile:`). You can find a list of these codes in [Resources](#resources).

![ide](/img/gitlab_ide.png)

Once you're done making your changes in the browser, you need to save them to the repo. This is called "committing." When you make a commit, be sure to add a descriptive message for what changes you made.

For example, `updated page and added images` is a bad description because it is vague. Something like `updated AH AMA, uploaded book and icon imgs` is much clearer. The goal of these commit messages is to create an informative log of what happened at each change, in case we need to revert to a past state of the site.

In the commit box, there are two options: (1) commit to trunk branch and (2) create a new branch. Option 1 will make the changes live on the website immediately after committing. Option 2 will create a separate branch (you can think of this as a draft), that someone else can look over and approve before the changes go live. We recommend going with Option 2 rather than pushing directly to "trunk" and asking someone on your team working on the page or in admin to take a look as well.

![commit](/img/gitlab_commit.png)

If at any point you need help, please reach out to `@Daisy Lu (she/her)` or `@John Boy [he/any]` on Slack.

## Resources <a name="resources"></a>

* [Markdown Basics](https://www.markdownguide.org/basic-syntax)
* [Emoji Short Text Codes](https://www.webfx.com/tools/emoji-cheat-sheet/)
